require('base64')

local STR_SIZE = 131072
local TRIES = 8192
local str = {}
for i = 1, STR_SIZE do
  str[i] = 'a'
end

local in_len = #str
local res = {}

enc(str, res)
print("encode " .. table.concat(str, "", 1, 4) .. "... to " .. table.concat(res, "",1, 4)  .. "...")

local t = os.clock()
local i = 0
local s = 0


while i < TRIES do
  s = s + enc(str, res)
  i = i +  1
end
print(os.clock() - t, s, i)
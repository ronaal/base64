local bit = require('bit')

local lshift = bit.lshift
local rshift = bit.rshift
local band   = bit.band

local byte   = string.byte
local floor  = math.floor

local b64table = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
  'w', 'x', 'y', 'z', '0', '1', '2', '3',
  '4', '5', '6', '7', '8', '9', '+', '/'
}

local mod_table = {0, 2, 1}

-- encoding
-- input and output should be table
-- The encoded base64 data is in output
-- @param input, a table of strings
-- @param output, a lua table(empty or not)
-- @return length of output
function enc(input, output) --both are tables
    local in_len  = #input
    local out_len = 4 * floor((in_len + 2) / 3) --calculate the length of output
    
    local i, j = 1, 1;
    
    local a, b, c, triple, v = 0, 0, 0, 0, 0;
    
    while i < in_len do
      a = i < in_len and byte(input[i]) or 0; i = i + 1;
      b = i < in_len and byte(input[i]) or 0; i = i + 1;
      c = i < in_len and byte(input[i]) or 0; i = i + 1;
      
      triple = lshift(a, 0x10) + lshift(b, 0x08) + c; 
      -- Base64 take in 3 8bit ascii input and convert it to 4 6bit number
      -- The 6bit output is picked from 2^6 b64table.
      
      v = band(rshift(triple, 18), 0x3F);
      output[j] = b64table[v + 1] ; j = j + 1;
      
      v = band(rshift(triple, 12), 0x3F);
      output[j] = b64table[v + 1] ; j = j + 1;
      
      v = band(rshift(triple,  6), 0x3F);
      output[j] = b64table[v + 1] ; j = j + 1;
      
      v = band(triple, 0x3F);
      output[j] = b64table[v + 1] ; j = j + 1; 
      
    end
    --Encode the zero padding added when i > input length
    for i = 1, mod_table[in_len % 3] do
      output[out_len - 1 - i] = '='
    end
    return out_len
end